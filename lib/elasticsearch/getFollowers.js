import client from './elasticConnect'
import dayjs from 'dayjs'

export default async (brand, filters) => {
  try {
    let body = {
      "aggs": {
        "2": {
          "date_histogram": {
            "field": "timestamp",
            "calendar_interval": "1d",
            "time_zone": "Europe/Berlin",
            "min_doc_count": 1
          },
          "aggs": {
            "1": {
              "sum": {
                "field": "metric_value"
              }
            }
          }
        }
      },
      "size": 0,
      "_source": {
        "excludes": []
      },
      "stored_fields": [
        "*"
      ],
      "script_fields": {},
      "docvalue_fields": [{
        "field": "timestamp",
        "format": "date_time"
      }],
      "query": {
        "bool": {
          "must": [],
          "filter": [{
              "bool": {
                "filter": [{
                    "bool": {
                      "should": [{
                        "match_phrase": {
                          "aggregated_type.keyword": "follower_count"
                        }
                      }],
                      "minimum_should_match": 1
                    }
                  },
                  {
                    "bool": {
                      "should": [{
                        "match_phrase": {
                          "brand_id.keyword": brand
                        }
                      }],
                      "minimum_should_match": 1
                    }
                  }
                ]
              }
            },
            {
              "range": {
                "timestamp": {
                  "format": "strict_date_optional_time",
                  "gte": dayjs().subtract(8, 'days'),
                  "lte": dayjs()
                }
              }
            }
          ],
          "should": [],
          "must_not": []
        }
      }
    }

    body.query.bool.filter.push(filters)
    
    let result = await client.search({
      index: 'brandmeter-elasticdayout',
      body: body
    })

    if(!result.body.aggregations) throw new Error('Didn\'t get a valid response from Elasticsearch', result)

    result = result.body.aggregations['2'].buckets.map(day => {
      day.value = day['1'].value
      day.date = day.key_as_string
      delete day.key_as_string
      delete day.doc_count
      delete day['1']
      delete day.key
      return day
    })

    // remove the last element, as it is the split bucket of today.
    result.pop()

    // calculate the change rate
    const changerate = Math.round((result[result.length - 1].value - result[0].value) / result[result.length - 1].value * 100)

    let response = {}

    response.changerate = changerate
    response.yesterday = result[result.length - 1].value
    response.merged_buckets = result


    return response


  } catch (error) {
    console.error('Error getting followers', error)
    return null

  }


}