import client from './elasticConnect'

export default async (brand, fromDate, toDate, filters) => {
    try {
        let body = {
            "aggs": {
                "2": {
                    "auto_date_histogram": {
                        "field": "timestamp",
                        "buckets": 80,
                        "format": "dd.MM.yyy",
                        "minimum_interval": "day",
                        "time_zone": "Europe/Berlin"
                    },
                    "aggs": {
                        "3": {
                            "terms": {
                                "field": "application.keyword",
                                "order": {
                                    "1": "desc"
                                },
                                "size": 5
                            },
                            "aggs": {
                                "1": {
                                    "sum": {
                                        "field": "metric_value"
                                    }
                                }
                            }
                        }
                    }
                },
                "total": {
                    "sum": {
                        "field": "metric_value"
                    }
                }
            },
            "size": 0,
            "_source": {
                "excludes": []
            },
            "stored_fields": [
                "*"
            ],
            "script_fields": {},
            "docvalue_fields": [{
                "field": "timestamp",
                "format": "date_time"
            }],
            "query": {
                "bool": {
                    "must": [],
                    "filter": [{
                            "bool": {
                                "filter": [{
                                        "bool": {
                                            "should": [{
                                                "match_phrase": {
                                                    "aggregated_type.keyword": "total_actions"
                                                }
                                            }],
                                            "minimum_should_match": 1
                                        }
                                    },
                                    {
                                        "bool": {
                                            "should": [{
                                                "match_phrase": {
                                                    "brand_id.keyword": brand
                                                }
                                            }],
                                            "minimum_should_match": 1
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "format": "strict_date_optional_time",
                                    "gte": fromDate,
                                    "lte": toDate
                                }
                            }
                        }
                    ],
                    "should": [],
                    "must_not": []
                }
            }
        }

        body.query.bool.filter.push(filters)

        let result = await client.search({
            index: 'brandmeter-elasticdayout',
            _source: false,
            body: body
        })

        if(!result.body.aggregations) throw new Error('Didn\'t get a valid response from Elasticsearch', result)


        let response = {}
        // setting the total value
        response.total = result.body.aggregations['total'].value
        response.interval = result.body.aggregations['2'].interval
        result = result.body.aggregations['2']

        let allnetworks = {}

        result = result.buckets.map(interval => {
            interval['3'].buckets.map(network => {
                // setting something like: { instagram: 56 }
                interval[network.key] = network['1'].value
                allnetworks[network.key] = true
            })
            interval.date = interval.key_as_string
            delete interval.key_as_string
            delete interval.key
            delete interval.doc_count
            delete interval['3']
            return interval
        })

        response.merged_buckets = result
        response.allnetworks = Object.keys(allnetworks)

        return response


    } catch (error) {
        console.error('Error getting Interactions', error)

    }



}