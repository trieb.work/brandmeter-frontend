import {
    Client
} from '@elastic/elasticsearch'

export default new Client({
    node: process.env.ELASTIC_NODE,
    auth: {
        username: process.env.ELASTIC_USERNAME,
        password: process.env.ELASTIC_PASSWORD
    }
});