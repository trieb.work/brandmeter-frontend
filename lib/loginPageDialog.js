import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button'
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import BusinessIcon from '@material-ui/icons/Business';
import CircularProgress from '@material-ui/core/CircularProgress';

import useSwr from 'swr'
import fetch from 'unfetch'
const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})

export default function Simpledialog(props){
    const { active, waiting, identyObject, onClose, brand, checked, setChecked } = props;

    let network = brand.network


    const { data, isValidating } = useSwr(active ? '/api/idpdata?dialogOpen=true&network='+network : null, fetcher)
   



    
    function handleClose() { 
        onClose();
    };
  
    const handleToggle = value => () => {
      const currentIndex = checked.indexOf(value);
      const newChecked = [...checked];
  
      if (currentIndex === -1) {
        newChecked.push(value);
      } else {
        newChecked.splice(currentIndex, 1);
      }
  
      setChecked(newChecked);
    }

    return (
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={active}>
            <DialogTitle id="simple-dialog-title">Select the accounts you want to add</DialogTitle>
            {isValidating && <LinearProgress />}
            { data && data.pages &&
                <List >
                    { data.pages.map(value => {
                        const labelId = `checkbox-list-label-${value.name}`;

                        return (
                        <ListItem key={value.name} role={undefined} dense button onClick={handleToggle(value)}>
                                <ListItemIcon>
                                    <Checkbox
                                        edge="start"
                                        checked={checked.indexOf(value) !== -1}
                                        tabIndex={-1}
                                        disableRipple
                                        inputProps={{ 'aria-labelledby': labelId }}
                                        /> 
                                    </ListItemIcon>
                                    <ListItemIcon>
                                    {value.type ==='instagram'?  <InstagramIcon /> : value.type === 'facebook' ? <FacebookIcon/> : 
                                            value.type === 'linkedin' ? <LinkedInIcon/> : value.type === 'googlemybusiness' ? <BusinessIcon/>:null}
                                    </ListItemIcon>
                                    
                                    <ListItemText id={labelId} primary={`${value.name}`} />

                                </ListItem>
                            
                    );
                })}
                </List> }
                <DialogActions style={{justifyContent: 'center'}}> 
                {identyObject && waiting ? <CircularProgress/> : <Button variant="contained" color="primary" onClick={handleClose}>
                        Confirm
                    </Button>}
                </DialogActions>

        </Dialog>

        )
    
}