import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'
import useSwr from 'swr'
import fetch from 'unfetch'
const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})



export function ShareDialog(props){
    const { active, setActive, activeBrand } = props
    const [query, setQuery] = React.useState('');
    const [value, setValue] = React.useState()
    const [error, setError] = React.useState('')
    const [loading, setLoading] = React.useState(false)
    const [options, setOptions] = React.useState([]);
    
    const { data, isValidating } = useSwr(active && query.length > 2 ? '/api/getusers?q='+query : '', fetcher)

    const handleChange = (event) => {
      setQuery(event.target.value);
    };

    const shareBrand = async () => {
        setLoading(true)
        let sharedWith = []
        if (activeBrand.sharedWith){
            sharedWith = activeBrand.sharedWith

            if (sharedWith.includes(value.id)) {
                setError('brand already shared with this user')
            }
        }
        if (!sharedWith.includes(value.id)) sharedWith.push(value.id)
        let result = await fetch('/api/settings/brands/'+activeBrand.id,{
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({sharedWith})

        })
        if (result.ok){
            setActive(false)
            setLoading(false)
            setValue(null)
            setQuery(null)

        } else {
            setLoading(false)
            setError('Something went wrong! Try again')
            setValue('')
            setQuery('')

        }




    }

    return(
        <Dialog open={active}>
            <DialogTitle>Share</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Share the brand "{ activeBrand.name }" with your team
                </DialogContentText>
                <Autocomplete
                    options={ isValidating ? [{nickname: '...loading'}] : data ? data : [] }
                    getOptionLabel={(option) => option.nickname}
                    id="auto-complete"
                    autoComplete
                    includeInputInList
                    value={value}
                    onChange={(event, newValue) => {
                    setValue(newValue);
                    }}
                    onInputChange={(event, newInputValue) => {
                       console.log(newInputValue);
                      }}
                    renderInput={(params) => <TextField {...params} error={error} helperText={error} label="Email Address or User ID" value={query} onChange={handleChange} margin="normal" />}
                />
            </DialogContent>
            <DialogActions>
                {!loading && <Button variant="contained" disabled={!value || loading } onClick={shareBrand} >Share</Button>}
                {loading && <CircularProgress  />}
                <Button onClick={() => {setQuery(''); setValue(''); setActive(false)}}> Close </Button>
            </DialogActions>


        </Dialog>

    )

}

// ToDo: Add an "shared with" section where users can see with whom they shared the brand with and delete Users
export function EditDialog(props){
    const { active, setActive, activeBrand, mutate } = props
    const [values, setValues] = React.useState({name: activeBrand.name, description: activeBrand.description})
    const [loading, setLoading] = React.useState(false)
    const onInput = (event) => {
        const value = event.target.value
        setValues({
            ...values,
            [event.target.name]: value
        })
    }

    // update the description when active brand changes
    React.useEffect(()=> {
        setValues({name: activeBrand.name, description: activeBrand.description})
    }, [activeBrand])

    const updateBrand = async () => {
        setLoading(true)
        let result = await fetch('/api/settings/brands/'+activeBrand.id,{
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(values)
        })
        if (result.ok){
            await mutate()
            setActive(false)
        }

    }
    return(
        <Dialog open={active}>
            <DialogTitle>Edit</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Edit the brand "{ activeBrand.name }"
                </DialogContentText>
                <Grid container direction="column" spacing="3">
                    <Grid item>
                        <TextField
                            label="Name"
                            value={values.name} 
                            onChange={onInput}
                            name="name"
                        />

                    </Grid>
                    <Grid item>
                        <TextField 
                            label="Description"
                            value={values.description}
                            onChange={onInput}
                            name="description"
                        />  

                    </Grid>

                </Grid>
           
            </DialogContent>
            <DialogActions>
                {!loading && <Button onClick={updateBrand} variant="contained">Save</Button>}
                {loading && <CircularProgress  />}
                <Button onClick={() => setActive(false)}>Cancel</Button>
            </DialogActions>

        </Dialog>
    )


}