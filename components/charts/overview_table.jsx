import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}



export default function SimpleTable({data, entity}) {
  const classes = useStyles();

  let rows = []
  for(const network in data.networks){
      rows.push({name: capitalize(network), totals: data.networks[network].total, change: (data.networks[network].changerate *100).toString().slice(0,5)}   )
  }
  rows.push({name: 'Total '+entity, totals: data.total, change: (data.changerate*100).toFixed(2).padStart(5,'0')})


  return (
    <Paper variant="outlined" elevation={0}> 
   
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align='left'>Network</TableCell>
            <TableCell align="right">Totals</TableCell>
            <TableCell align="right">% Change</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name === 'Total '+entity ? <Typography variant="h6"> {row.name} </Typography>   : row.name}
              </TableCell>
              <TableCell align="right">{row.name === 'Total '+entity ? <Typography style={{fontWeight: "bold"}}> {row.totals} </Typography>   : row.totals}</TableCell>
              <TableCell align="right">{row.name === 'Total '+entity ? <Typography style={{fontWeight: "bold"}}>{row.change} % </Typography> : row.change+' %' }</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    
    </Paper>
  );
}
