import Card from '@material-ui/core/Card'
import Divider from '@material-ui/core/Divider' 
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid'
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import { makeStyles } from '@material-ui/core/styles';
import dayjs from 'dayjs'



const useStyles = makeStyles({
    media: {
      height: 140,
    },
    card: {
        minWidth: 180
    }
  });


export default ({data, icons}) => {
    const classes = useStyles();


    return(
        <Card variant='outlined' >
            <CardHeader
                title='Top Posts & Stories'
                subheader='Review your top posts and stories published during the selected time period, based on the post or story’s lifetime performance.'
            />
            <Divider/>
            <CardContent>
                {/* for every top-post one grid item */}
                <Grid container spacing={3}>
                    {
                        data && data.posts && data.posts.topPosts.length > 0 ? data.posts.topPosts.slice(0, 4).map(post => (
                            <Grid item sm={3} xs={3} xl={2} key={post.post_id+'Grid'}>
                                <Card key={post.post_id} className={classes.card}>
                                    <CardMedia
                                        className={classes.media}
                                        image={post.s3url ? post.s3url : post.media_url}
                                        component="img"                                
                                    />
 
                                    <CardHeader 
                                        title={ dayjs(post.timestamp).format('DD.MM.YY') }
                                        subheader={post.application +  (post.post_type === 'story' ? ' Story':' Post')}
                                        avatar={icons[post.application].icon}
                                    />
                               
                                    <CardContent>
                                        <Divider/>
                                        <Table size="small" >
                                            <TableBody>
                                                <TableRow key={post.post_id +'Engagements'}>
                                                    <TableCell style={{paddingLeft: "0px"}} align="left" colSpan={2}> Engagements </TableCell>
                                                    <TableCell style={{paddingRight: "0px"}} align="right"> {post.engagement}</TableCell>
                                                </TableRow>
                                                <TableRow key={post.post_id +'Impressions'}>
                                                    <TableCell style={{paddingLeft: "0px"}} align="left" colSpan={2}> Impressions</TableCell>
                                                    <TableCell style={{paddingRight: "0px"}} align="right"> {post.impressions}</TableCell>
                                                </TableRow>
                                                <TableRow key={post.post_id +'engagement_rate'}>
                                                    <TableCell style={{paddingLeft: "0px"}} align="left" colSpan={2}> Engagement Rate</TableCell>
                                                    <TableCell style={{paddingRight: "0px"}} align="right"> {Math.round(post.engagement_rate * 100)}%</TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
        
                                    </CardContent>
                                    <CardActions>
                                        <Button onClick={()=> window.open(post.post_type === 'story' ? post.media_url : post.permalink, "_blank")}>View Post</Button>
                                    </CardActions>
                                </Card>
                                
                            </Grid>

                        )) : <Grid item> <Typography> No posts in this time period</Typography> </Grid>
                    }

                </Grid>

            </CardContent>
        </Card>
    )

}