import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';

 



export const StatsCard = ({data, classes}) => {

    return(
        <Card className={classes.card}>
       
        <CardContent>
            <Grid container direction="column" alignItems="center" spacing={1} >
                <Grid item>
                        <Grid container direction="column" alignItems="center">
                            <Grid item>
                                <Typography variant="body2">Total Followers</Typography>
                            </Grid> 
                            {data && data.totalFollowers ?
                            <Grid item>
                                <Grid container  direction="row" alignItems="center"> 
                                    <Grid item>
                                        <Typography variant="h6" align="right">
                                            {data.totalFollowers.yesterday}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <ArrowDropUpIcon style={{ color: green[500] }} fontSize="small"/>
                                    </Grid>
                                
                                </Grid>
                            </Grid>
                                : <CircularProgress fontSize="small"/>}
                        </Grid> 
                   
                </Grid>
                <Grid item>
                    <Grid container direction="column" alignItems="center">
                        <Grid item>
                            <Typography variant="body2">Interactions</Typography>
                        </Grid>
                            {data && data.interactions ?
                            <Grid item>
                                <Typography variant="h6" >
                                    {data.interactions.total} 
                                </Typography>
                            </Grid>
                         : <CircularProgress />}
                        </Grid>
                </Grid>
                <Grid item>
                    <Grid container alignItems="center">    
                        <Grid item>
                            <Typography variant="body2">Engagement Rate</Typography>
                        </Grid>
                        <Grid item>
                        {data && data.engagementRate ?
                            <Typography variant="h6" >
                                {data.engagementRate.value} %
                                </Typography> : <CircularProgress />}
                        </Grid>
                    </Grid> 
                </Grid>

            </Grid>
            </CardContent>
            
        </Card>
    )
}

