import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import { DateRangePicker, DateRange, DateRangeDelimiter } from "@material-ui/pickers";
import { LocalizationProvider } from '@material-ui/pickers';
import dayjsUtils from '@material-ui/pickers/adapter/dayjs'

import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import BusinessIcon from '@material-ui/icons/Business';
import InstagramIcon from '@material-ui/icons/Instagram';
import FilterListIcon from '@material-ui/icons/FilterList';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import dayjs from 'dayjs'
import createPersistedState from 'use-persisted-state';
const useActiveBrand = createPersistedState('activeBrand')


const colorAndIcons = {
    facebook: {
        icon: <FacebookIcon />,
        color: '#FC5C65'
    },
    instagram: {
        icon: <InstagramIcon />,
        color: '#FD9644'
    },  
    linkedin: {
        icon: <LinkedInIcon />,
        color: '#FED330'
    },
    googlemybusiness: {
        icon: <BusinessIcon />,
        color: '#26DE81'
    }
}



export default function pickerandfilter({selectedDate, handleDateChange, setcheckedPages, demographics = false, setActiveTab}) {
    
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [activeBrand, setActiveBrand] = useActiveBrand({})
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    
    const handleClose = () => {
        setAnchorEl(null);
    };


    const [allNetworks, setAllNetworks] = React.useState( () =>  activeBrand && activeBrand.activePages ? Object.values(activeBrand.activePages).flat() : [] )

    React.useEffect(() => {
        if(activeBrand.brand) setAllNetworks( Object.values(activeBrand.activePages).flat() )
    },[activeBrand])

    React.useEffect(() => {
        let newArr = allNetworks.map( page => page.id )
        setcheckedPages(newArr)

    }, [allNetworks])


    const handleEnabledNetworksChange = index => event => {
        let newArr = [...allNetworks]
        newArr[index].unchecked = !event.target.checked
        setAllNetworks( newArr )

    }
    const [value, setValue] = React.useState(0);

    const handleTabChange = (event, newValue) => {
        setValue(newValue);
        setActiveTab(newValue)
    };

    return(
        <LocalizationProvider dateAdapter={dayjsUtils}>
            <Grid container spacing={2} alignItems="flex-end" justify="space-between">
                <Grid item item xs={5} sm={5} md={3} lg={4}>
                
                { demographics && <Tabs
                    value={value}
                    onChange={handleTabChange}
                    indicatorColor="primary"
                    textColor="primary"
                >
                    <Tab label="Performance" />
                    <Tab label="Demographics" />
                </Tabs>}


                </Grid>
                <Grid item xs={6} sm={6} md={4} lg={3}>
                <DateRangePicker
                    startText="Start-Date"
                    endText="End-Date"
                    value={selectedDate}
                    onChange={date => handleDateChange(date)}
                    renderInput={(startProps, endProps) => (
                        <>
                        <TextField {...startProps} />
                        <DateRangeDelimiter> to </DateRangeDelimiter>
                        <TextField {...endProps} />
                        </>
                    )}
                    />
                                
                </Grid>
               
                <Grid item xs={1} sm={1} md={1} lg={1}>
    
                    <IconButton 
                        aria-controls="simple-menu"
                        aria-label="filter list" 
                        onClick={handleClick}
                        >
                        <FilterListIcon />
                    </IconButton>
                    <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                    { allNetworks && allNetworks.length > 0 && allNetworks.map( (page, index) =>
                        <MenuItem key={page.id}>
                            <ListItemIcon>
                                {colorAndIcons[page.type].icon}
                            </ListItemIcon>
                            <FormGroup >
                                <FormControlLabel
                                    control={<Checkbox checked={!page.unchecked} onChange={handleEnabledNetworksChange(index)}   />}
                                    label={page.name}
                                    
                                />
                            </FormGroup>
                    </MenuItem>
                

                    )}
                    </Menu>
            
                

                </Grid>
            </Grid>
        </LocalizationProvider>

    )

}