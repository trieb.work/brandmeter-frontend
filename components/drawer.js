import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';


import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PublicIcon from '@material-ui/icons/Public';
import AllInboxIcon from '@material-ui/icons/AllInbox';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { useRouter } from 'next/router'
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import BusinessIcon from '@material-ui/icons/Business';
import InstagramIcon from '@material-ui/icons/Instagram';
import CachedIcon from '@material-ui/icons/Cached';
import AddIcon from '@material-ui/icons/Add';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import LinearProgress from '@material-ui/core/LinearProgress';


import createPersistedState from 'use-persisted-state';
const useActiveBrand = createPersistedState('activeBrand');
const useSwitchBrandDialog = createPersistedState('switchBrand')

import useSwr from 'swr'
import fetch from 'unfetch'

const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})


import { useFetchUser } from '../lib/user'
import { Typography } from '@material-ui/core';


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  logo: {
    maxWidth: 200
  },
  hide: {
    opacity: 0,
    pointerEvents: 'none'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  drawertoolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

export default function MiniDrawer( { drawerEnabled = true } ) {
  const router = useRouter()
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [anchorProfileMenuEl, setAnchorProfileMenuEl] = React.useState(null);
  const isProfileMenuOpen = Boolean(anchorProfileMenuEl);
  const [switchBrandopen, setswitchBrandopen] = useSwitchBrandDialog(false);
  const [switchBrandselectedValue, setswitchBrandSelectedValue] = React.useState({});
  const [activeBrand, setActiveBrand] = useActiveBrand({})

  const { data } = useSwr( '/api/settings/brands', fetcher )


  const handleswitchBrandOpen = () => {
    setswitchBrandopen(true);
  };

  const handleswitchBrandClose = (value) => {
    if (value.mode === 'addbrand') return router.push('/addbrand')

    setswitchBrandSelectedValue(value);
    setActiveBrand(value)
    setswitchBrandopen(false)
    setAnchorProfileMenuEl(null)
  };
  
  const { user, loading } = useFetchUser()


  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleProfileMenuOpen = event => {
    setAnchorProfileMenuEl(event.currentTarget);
  };
  const handleProfileMenuClose = (link, event) => {
    setAnchorProfileMenuEl(null);
  };

  const Icons = {
    facebook: {
        icon:<FacebookIcon />,
      },
    instagram: {
        icon:<InstagramIcon />,
    },  
    linkedin: {
        icon:<LinkedInIcon/>,
    },
    googlemybusiness: {
        icon:<BusinessIcon />,
    }
  }


  const ProfilemenuId = 'primary-search-account-menu';

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar className={classes.toolbar}>
        {user && drawerEnabled ? <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton> :
            <IconButton
            className={classes.hide}
          >
            <MenuIcon />
          </IconButton> } 
          <img src="/brandmeter_logo_text.svg" alt="logo" className={classes.logo} />
          { user && <Typography onClick={handleswitchBrandOpen} style={{position: "absolute", right:"60px", cursor: "pointer"}} > {activeBrand && activeBrand.name ? activeBrand.name : 'Please activate a brand'}</Typography>}
          <IconButton
            edge="end"
            aria-label="account of current user"
            aria-controls={ProfilemenuId}
            aria-haspopup="true"
            onClick={handleProfileMenuOpen}
            color="inherit"
          >
              <AccountCircle />
          </IconButton>
          <Menu
            anchorEl={anchorProfileMenuEl}
            id={ProfilemenuId}
            open={isProfileMenuOpen}
            onClose={handleProfileMenuClose}
            >
              {user && <MenuItem onClick={() => {router.push('/profile'); handleProfileMenuClose() }}>My Account</MenuItem>}
              <Divider />
              {user && <MenuItem onClick={handleswitchBrandOpen}><ListItemIcon><CachedIcon /> Switch Brand </ListItemIcon>  </MenuItem>}
              { user && <MenuItem onClick={() => {router.push('/addbrand')}}><ListItemIcon><AddIcon /> Add Brand </ListItemIcon>  </MenuItem>}
              <Divider />
            {user ? <MenuItem onClick={() => {router.push('/api/logout'); handleProfileMenuClose()}}>Logout</MenuItem> : <MenuItem onClick={() => router.push('/api/login')}>Login</MenuItem>}
          </Menu>
          <SwitchBrandDialog selectedValue={switchBrandselectedValue} open={switchBrandopen} onClose={handleswitchBrandClose} />

       </Toolbar>

      </AppBar>
      { user && drawerEnabled && 
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.drawertoolbar}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />
          <List>
            <ListItem disabled={!data || data.length < 1} button key="Brand Overview" onClick={ () => router.push('/overview') }>
              <ListItemIcon><PublicIcon/></ListItemIcon>
              <ListItemText primary="Brand Overview"/>
            </ListItem>
            <ListItem disabled={!data || data.length < 1} button key="Post Performance" onClick={ () => router.push('/posts') }>
              <ListItemIcon><AllInboxIcon /></ListItemIcon>
              <ListItemText primary="Post Performance" />
            </ListItem> 
          </List>
          <Divider />
          {/*  To be implemented. Right now we don't have any sub-pages per network only */}
          {/* <List>
            {user && user.app_metadata && Object.keys(user.app_metadata.pages).map((network) => 
              <ListItem button key={network}>
                <ListItemIcon key={network}>{ Icons[network].icon }</ListItemIcon>
                <ListItemText primary={network === 'googlemybusiness' ? 'G-MyBusiness Report' : capitalize(network) + ' Report'} />
              </ListItem>
                )}
          </List> */}
        </Drawer>
      }
    </div>
  );
}




function SwitchBrandDialog(props) {

  const { onClose, selectedValue, open } = props;


  const { data } = useSwr( open ? '/api/settings/brands' : null, fetcher )

  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleListItemClick = (value) => {
    onClose(value);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">Select Brand</DialogTitle>
      { !data && <LinearProgress /> }
      <List>
        {
          data && data.length > 0 ? data.map( (brand) => (
            <ListItem button key={brand.name} onClick={() => handleListItemClick(brand)}>
              <ListItemText primary={brand.name} />
            </ListItem>
            
          )) : 
          <ListItem>
            <ListItemText primary="You don't have any brands yet. Please add them in the settings first."/>
          </ListItem>
        }

        <ListItem autoFocus button onClick={() => handleListItemClick({mode: 'addbrand'})}>
          <ListItemAvatar>
            <Avatar>
              <AddIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Add Brand"  />
        </ListItem>
      </List>
    </Dialog>
  );
}

SwitchBrandDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.object.isRequired,
};


const capitalize = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
}