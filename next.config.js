const withPlugins = require('next-compose-plugins');
const dotenv = require('dotenv')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
})
dotenv.config()

const withSourceMaps = require('@zeit/next-source-maps')()

// Use the SentryWebpack plugin to upload the source maps during build step
const SentryWebpackPlugin = require('@sentry/webpack-plugin')
const {
  SENTRY_DSN,
  SENTRY_ORG,
  SENTRY_PROJECT,
  SENTRY_AUTH_TOKEN,
  NODE_ENV,
} = process.env

// next.js configuration
const nextConfig = {
    poweredByHeader: false,
    webpack: (config, options) => {
      if (!options.isServer) {
        config.resolve.alias['@sentry/node'] = '@sentry/browser'
      }
      if (
        SENTRY_DSN &&
        SENTRY_ORG &&
        SENTRY_PROJECT &&
        SENTRY_AUTH_TOKEN && 
        NODE_ENV === 'production'
      ) {
        config.plugins.push(
          new SentryWebpackPlugin({
            release: options.buildId,
            include: '.next',
            ignore: ['node_modules'],
            urlPrefix: '~/_next',
          })
        )
      }
      config.module.rules.push({
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      })
      return config
    },
    env: {
      AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
      AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
      AUTH0_CLIENT_SECRET: process.env.AUTH0_CLIENT_SECRET,
      AUTH0_SCOPE: 'openid profile',
      REDIRECT_URI:
        process.env.REDIRECT_URI || '/api/callback',
      POST_LOGOUT_REDIRECT_URI:
        process.env.POST_LOGOUT_REDIRECT_URI || '/',
      SESSION_COOKIE_SECRET: process.env.SESSION_COOKIE_SECRET,
      SESSION_COOKIE_LIFETIME: 7200, // 2 hours,
      ELASTIC_NODE: process.env.ELASTIC_NODE,
      ELASTIC_USERNAME: process.env.ELASTIC_USERNAME,
      ELASTIC_PASSWORD: process.env.ELASTIC_PASSWORD,
      S3_URL: process.env.S3_URL,
      S3_ACCESS_KEY: process.env.S3_ACCESS_KEY,
      S3_SECRET_KEY: process.env.S3_SECRET_KEY,
      S3_BUCKET_NAME: process.env.S3_BUCKET_NAME,
      SENTRY_DSN: process.env.SENTRY_DSN
    },
    reactStrictMode: false  
}

module.exports = withPlugins([
  [withSourceMaps],
  [withBundleAnalyzer]
], nextConfig

)
