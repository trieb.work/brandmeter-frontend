// This API route is used to communicate with the AUTH0 API, to merge different Identity Providers like google, facebook etc. of a user 
// and to get additional information on that user form the auth0 backend API

import auth0 from '../../lib/auth0'
import getPages from '../../lib/pages'
import management from '../../lib/auth0management'

export default async function idpdata(req, res) {

    try {
        let answer = await auth0.getSession(req)
        if (!answer) return res.status(401).json({
            status: 'Not authenticated'
        })


        // check if we have a valid user in general
        if (answer.user) {


            // check if we need to merge users (if we have a special state in the cookie)
            if (req.cookies['a0:state']) {
                try {
                    const state = JSON.parse(req.cookies['a0:state'])
                    if (state.mergeuser) {

                        // it might happen, that we already merged the user with a different identity. We search
                        // now in auth0 if this ID is included in a different user. With this search we get the "main" user_id
                        const search = 'identities.user_id:' + '"' + state.mergeuser.split('|')[1] + '"'

                        const main_user = await management.getUsers({
                            q: search,
                            search_engine: 'v3'
                        })

                        if (main_user && main_user[0] && main_user[0].user_id !== answer.user.sub) {
                            console.log('Trying to link now', main_user[0].user_id, 'with', answer.user.sub)
                            // call the management API with the current and the mergeuser to link them
                            await management.linkUsers(main_user[0].user_id, {
                                user_id: answer.user.sub,
                                provider: answer.user.sub.split('|')[0]
                            })
                        } else {
                            console.log('We don\'t want to merge. The two ids we got:', main_user[0].user_id, answer.user.sub)
                        }



                    }
                } catch (error) {
                    if (error.statusCode === 409) {
                        console.log('We already merged this user, everything fine')
                    } else {
                        console.error('Error merging different auth0 ids.', error)

                    }

                }


            }

            // call the auth0 management API to get the data of the current user
            const search = 'identities.user_id:' + '"' + answer.user.sub.split('|')[1] + '"'

            const result = await management.getUsers({
                q: search,
                search_engine: 'v3'
            })

            
            // for all of the different identities we need to check the available pages or properties
            // we just need this when the setup dialog is open in the frontend
            if(req.query.dialogOpen && req.query.network) {
                let filter
                req.query.network === 'google' ? filter = 'google-oauth2' : filter = req.query.network
                let neededIdentity = result[0].identities.filter( x => x.provider === filter)
                result[0].pages = await getPages(neededIdentity)
            }



            return res.status(200).json(result[0])
        }
        return res.status(401).json({
            status: 'Not authenticated'
        })

    } catch (error) {
        console.log(error)
        res.status(error.status || 500).end(error.message)

    }
}