import auth0 from '../../../../lib/auth0'
import client from '../../../../lib/elasticsearch/elasticConnect'
import assert from 'assert'





export default auth0.requireAuthentication(async (req, res) => {
    const { method } = req
    const user = (await auth0.getSession(req)).user
    const { id } = req.query

    
    if (method === 'POST'){
        let result = await client.get({
            index: 'brandmeter-user-data',
            id: id
        })
        const brand = result.body._source

        
        // check if this user is really owning the brand or brand is shared with user
        if (brand.auth0id === user.sub ||  brand.sharedWith.includes(user.sub)){
            let result = await client.update({
                index: 'brandmeter-user-data',
                refresh: true,
                id,
                body: {doc: req.body} 

            })
            assert.strictEqual(result.statusCode,200)

            return res.status(200).json({message: 'brand updated'})




        }

       
    }

    if (method === 'DELETE'){
        let result = await client.get({
            index: 'brandmeter-user-data',
            id: id
        })
        const brand = result.body._source
        
        // check if this user is really owning the brand
        if (brand.auth0id === user.sub ){
            console.log('deleting the brand', brand.name, 'with ID', result.body._id)
            result = await client.delete({
                index: 'brandmeter-user-data',
                id: id
            })
            assert.strictEqual(result.statusCode, 200)
            await client.deleteByQuery({
                index: 'brandmeter-*',
                body:{
                    query:{
                        match:{
                            "brand_id": id
                        }
                    }

                }

            })

            return res.status(200).json({message: 'brand and corresponding data deleted'})
        }
        return res.status(400).json({message: 'You are not allowed to delete this brand'})
    }

})