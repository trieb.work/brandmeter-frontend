import auth0 from '../../lib/auth0'
import management from '../../lib/auth0management'





export default auth0.requireAuthentication(async (req, res) => {

    if (!req.query || !req.query['q']){
        throw new Error('Query parameter "q" missing or not valid')
    }

    const users = await management.getUsers({
        q: `"${req.query['q']}"`,
        search_engine: 'v3'
    })
    let returnValue = users.map(user => {
            return {nickname: user.nickname, id: user.user_id}
    } )
    return res.status(200).send(returnValue)


    




})