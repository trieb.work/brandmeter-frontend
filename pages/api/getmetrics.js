import auth0 from '../../lib/auth0'
import getAggregatedType from '../../lib/elasticsearch/getAggregatedType'
import getFollowers from '../../lib/elasticsearch/getFollowers'
import getInteractions from '../../lib/elasticsearch/getInteractions'
import getEngagementRate from '../../lib/elasticsearch/getEngagementRate'

import dayjs from 'dayjs'



// this route is used to pull data from Elasticsearch and to bring it into the right format


export default auth0.requireAuthentication(async function getmetrics(req, res) {

    const {
        query: {
            fromDate,
            toDate,
            networks,
            fields,
            brand
        }
    } = req;
    let user = await auth0.getSession(req)
    user = user.user

    let response = {}
    try {
        //check inputs,
        if (!(new Date(fromDate) <= new Date())) throw new Error("invalid input fromDate"); //if provided date is not a date or in the future
        if (!(new Date(toDate) <= new Date())) throw new Error("invalid input toDate");
        if (!(new Date(fromDate) <= new Date(toDate))) throw new Error("fromDate should be lower as toDate");
        if (!brand) throw new Error('No brand inn querystring found. Please add one.')

        
        let filters = []

        if (networks && networks !=='undefined') {
            // filter for only enabled networks
            let enabledPages = JSON.parse(networks)
    

            for(const page of enabledPages){
              if(page === undefined) continue
              filters.push({
                "match_phrase": {
                  "page_id.keyword": page
                }
              })
            }
            filters = { "bool":  { "should": filters, "minimum_should_match": 1 }} 
            
            
        } else{
            // no enabled pages. We send back a valid response, thats all
            return res.status(200).send(response)

        }



        // reset the time in date to 00:00
        let date = {  
            toDate, 
            fromDate: dayjs(fromDate).startOf('day').toISOString()
        }
        // create the timestamps for the compare time-period
        // to calculate the change rate we need to first calculate the time-period we compare with
        const differenceInDays = dayjs(toDate).diff(dayjs(fromDate), 'days')
        date.compareTimefromDate = dayjs(fromDate).subtract(differenceInDays, 'days').toISOString()
        date.compareTimetoDate = fromDate


        // run only the queries that are enabled. If nothing specified, run them all
        const enabledFunctions = fields ? fields.split(',') : ['impressions','totalFollowers','interactions','engagementRate','posts']

 
      
        let aggregate_type_metrics = await getAggregatedType(brand, date, filters)
            Object.assign(response, aggregate_type_metrics)

        if(enabledFunctions.includes('interactions')){
            let interactions = getInteractions(brand, fromDate, toDate, filters)
            response.interactions = await interactions
        }

        if(enabledFunctions.includes('engagementRate')){
            let engagementRate = getEngagementRate(brand, fromDate, toDate, filters)
            response.engagementRate = await engagementRate
        }
        
        

        
        return res.status(200).send(response)

    } catch (error) {

        console.log('Error getting Elasticsearch Metrics', error)
        console.log(response)
        return res.status(500).json({
            'Message': error
        })

    }




})