import Drawer from '../components/drawer'
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { green } from '@material-ui/core/colors';
import { useEffect } from 'react'
import { useRouter } from 'next/router'


import useSwr from 'swr'
import fetch from 'unfetch'

const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})


import {useFetchUser} from '../lib/user'



const useStyles = makeStyles(theme => ({
    root:{
        display: "flex",
        padding: theme.spacing(2)
    },
    chartPaper:{
        padding: theme.spacing(2)
    },
    cardHeader:{
        title: {
            fontSize: "10rem"
        },
        subheader: {
            fontSize: 12
        }
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    card: {
        maxWidth: "9rem",
        height: "10rem"
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25ch',
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '3%',
        marginTop: -12,
        marginLeft: -12,
    },
    // needed to shift content below appbar
    toolbar: theme.mixins.toolbar

}));




export default () => {
    const classes = useStyles();
    const { user, loading } = useFetchUser({required: true})
    const [inputs, setInputs] = React.useState({Name:'', Description: ''})
    const [error, setError] = React.useState(false)
    const [submitLoading, setSubmitLoading] = React.useState(false)
    const { data } = useSwr( '/api/settings/brands', fetcher )
    const router = useRouter()


    const onInput = (event) => {
        const value = event.target.value
        setInputs({
            ...inputs,
            [event.target.name]: value
        })
    }
    useEffect(() => {
        if(data){
            let result = data.find( x => x.name === inputs.Name)
            result ? setError(true) : setError(false) 
        }
   
    }, [inputs])
    
    const addBrand = async () => {
        setSubmitLoading(true)
        let result = await fetch('/api/settings/brands',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({name: inputs.Name, description: inputs.Description})
            
        })
        result.ok ? router.push('/profile') : ''
        return true
    
    }



    return(
        <>
            <Drawer />
            <div className={classes.toolbar} /> 
            <Box>
                <div className={classes.root}>
                <Container maxWidth="md">
                    <Card variant='outlined'>
                        <CardHeader
                            className={classes.cardHeader}
                            title='Add a new brand'
                            subheader='Create a new brand for your company and connect associated social media accounts.'
                        />
                        <Divider/>
                        <CardContent>
                            <TextField label="Name" placeholder="Acme Inc." fullWidth margin="normal" required error={error} helperText={ error ? "You already have a brand with this name" : ''} value={inputs.Name} name="Name" onChange={onInput} />
                            <TextField label="Description" placeholder="Brand Account of Acme Inc." fullWidth margin="normal" value={inputs.Description} name="Description" onChange={onInput}/>

                            <div className={classes.wrapper}>
                                <Button onClick={addBrand} variant='contained' color='primary' disabled={ !inputs.Name || submitLoading ? true : false}>Save brand </Button>
                                {submitLoading && <CircularProgress size={24} className={classes.buttonProgress}/> }
                            </div>
                        </CardContent>
                       
                          
                    </Card>

                </Container>
                </div>
            </Box>
        </>
    )
} 

