import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Drawer from '../components/drawer'
import Box from '@material-ui/core/Box';
import Backdrop from '@material-ui/core/Backdrop';
import Footer from '../components/Footer'


import { useFetchUser } from '../lib/user'
import { useRouter } from 'next/router'

import useSwr from 'swr'
import fetch from 'unfetch'

const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  // needed to shift content below appbar
  toolbar: theme.mixins.toolbar

}));


function Home() {
  const { user, loading } = useFetchUser()
  const router = useRouter()

  const { data } = useSwr( '/api/settings/brands', fetcher )




  const classes = useStyles();


  return (
    <>
    <Drawer drawerEnabled={false}/>
       {/* the fullscreen loading overlay */}
      <Backdrop className={classes.backdrop} open={loading}>
        <img src='/logoanimation_klein.png' />
      </Backdrop>
    <div className={classes.toolbar} /> 
    <Box m={3}>
      <div className={classes.root}>
        <Container maxWidth="md">
          {!user && 
              <Grid container spacing={4} justify="center" >
                <Grid item md={8}>
                      <Typography> 
                        Welcome, you have found BRANDMETER, the ultimate social media analytics service.
                        Please register or login.
                        You can test-drive the service for free during the open development time. The project is fully open source.

                      </Typography>
                    
                      {!loading ? <Button onClick={() => router.push('/api/login')} variant="contained" color="primary">
                        Register / Login
                      </Button> : <CircularProgress/> }

                </Grid>
              </Grid>
          }
      

          {user && data && data.length > 0 && (
            <Grid container spacing={3} justify="center">
              <Grid item md={8}>
                <Paper className={classes.paper}>
                  <Typography variant="h5"> 
                      Go to your brand's dashboard  
                  </Typography>
                      <Button onClick={() => router.push('/overview')} variant="contained" color="primary">
                        dashboard
                      </Button>
                  </Paper>
                </Grid>
            </Grid>
          )}

          { user && data && data.length < 1 && (
                        <Grid container spacing={3} justify="center">
                        <Grid item md={8}>
                          <Paper className={classes.paper}>
                            <Typography variant="h5"> 
                                Welcome!
                            </Typography>
                            <Typography>
                              You are a new user without any brands created yet. Just start out by creating a brand or tell your colleagues to share 
                              an existing brand with you. 
                            </Typography>
                                <Button onClick={() => router.push('/addbrand')} variant="contained" color="primary">
                                  Add brand
                                </Button>
                            </Paper>
                          </Grid>
                      </Grid>



          )}

        </Container>
      </div>
      </Box>
      <Footer /> 
    </>
  )
}

export default Home
