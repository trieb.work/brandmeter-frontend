import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';

import Button from '@material-ui/core/Button'
import PropTypes from 'prop-types';
import TableContainer from '@material-ui/core/TableContainer';

import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import BusinessIcon from '@material-ui/icons/Business';
import InstagramIcon from '@material-ui/icons/Instagram';

import Backdrop from '@material-ui/core/Backdrop';
import dayjs from 'dayjs'
import useSWR from 'swr'
import fetch from 'unfetch'

import Drawer from '../components/drawer'
import Pickersandfilter from '../components/pickerandfilter'
import { useFetchUser } from '../lib/user'
import { CardMedia, Typography } from '@material-ui/core';

import createPersistedState from 'use-persisted-state';
const useActiveBrand = createPersistedState('activeBrand')
const useSwitchBrandDialog = createPersistedState('switchBrand')

const fetcher = url => fetch(url).then(r => r.json()).catch(err => {throw err})

const colorAndIcons = {
    facebook: {
        icon: <FacebookIcon />,
        color: '#FC5C65'
    },
    instagram: {
        icon: <InstagramIcon />,
        color: '#FD9644'
    },  
    linkedin: {
        icon: <LinkedInIcon />,
        color: '#FED330'
    },
    googlemybusiness: {
        icon: <BusinessIcon />,
        color: '#26DE81'
    }
}

function descendingComparator(a, b, orderBy) {
    if(orderBy === 'post'){
        if (b[orderBy]['value'] < a[orderBy]['value']) {
            return -1;
          }
          if (b[orderBy]['value'] > a[orderBy]['value']) {
            return 1;
          }
          return 0;

    }
    
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }
   
  
function getComparator(order, orderBy) {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
}
  
function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}
const headCells = [
    { id: 'post', alignLeft: true, disablePadding: false, label: 'Post (by date)' },
    { id: 'network', disablePadding: false, label: 'Network' },
    { id: 'type', disablePadding: false, label: 'Type' },
    { id: 'engagement_rate', disablePadding: false, label: 'Engagement rate' },
    { id: 'impressions', disablePadding: false, label: 'Impressions' },
    { id: 'engagements', disablePadding: false, label: 'Engagements' }
  ];
function EnhancedTableHead(props) {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={headCell.alignLeft ? 'left':'right'}
              padding={headCell.disablePadding ? 'none' : 'default'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                <Typography noWrap>{headCell.label}</Typography>
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
}
  
EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired
};




const useStyles = makeStyles(theme => ({
    root:{
        display: "flex"
    },
    chartPaper:{
        padding: theme.spacing(2)
    },
    cardHeader:{
        title: {
            fontSize: "10rem"
        },
        subheader: {
            fontSize: 12
        }
    },
    table: {
        minWidth: 750,
      },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    fab: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
    content: {
        padding: theme.spacing(2),
        flexGrow: 1
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    card: {
        maxWidth: "9rem",
        height: "10rem"
    },
    media:{
        height: "5rem"
    },
    // needed to shift content below appbar
    toolbar: theme.mixins.toolbar

}));





export default function Posts() {
    const classes = useStyles();

    const [anchorPagesMenu, setAnchorPagesMenu] = React.useState(null);
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('engagement_rate');
    const [selectedDate, handleDateChange] = React.useState([dayjs().subtract(1, 'month'), dayjs().subtract(1, 'days')]);

    const { user, loading } = useFetchUser({required: true})

    const [checkedPages, setcheckedPages] = React.useState({})

    const [activeBrand, setActiveBrand] = useActiveBrand({})
    const [switchBrandopen, setswitchBrandopen] = useSwitchBrandDialog(() => activeBrand.brand ? false : true);

    // you need to select a brand if you did not now
    React.useEffect(() => {
        if (!activeBrand.brand) setswitchBrandopen(true)
    }, [activeBrand] )

   
    const { data } = useSWR(checkedPages ? '/api/getposts?fromDate='+selectedDate[0].toISOString()+'&toDate='+selectedDate[1].toISOString()+'&networks='+encodeURIComponent(JSON.stringify(checkedPages))+'&fields=posts'+'&brand='+activeBrand.id : '', fetcher)

    let rows = []
    if(data && data.posts && data.posts.topPosts){
        for (const post of data.posts.topPosts){
            const card = (
                <Grid container spacing={1}>
                    <Grid item xs={3} sm={4}>
                        <Card className={classes.card} variant="outlined">
                            <CardMedia
                                image={post.s3url}
                                component="img"  
                                className={classes.media}
                            />
                            <CardContent>
                                <Typography align="center">
                                    {dayjs(post.timestamp).format('DD.MM.YYYY')}
                                </Typography>
                            </CardContent>
                        </Card>
                        
                    </Grid>
                    <Grid item sm={8} style={{height: "10rem", width:"100%"}}>
                        <Box textOverflow="ellipsis" overflow="hidden" style={{height: "8rem"}}>
                            {post.caption}
                        </Box>
                        <Button onClick={()=> window.open(post.post_type === 'story' ? post.media_url : post.permalink, "_blank")}>View Post</Button>
                    </Grid>
                </Grid>
              
            )
            let data = {card, value: post.timestamp}
            rows.push({post:data,network: post.application, type: (post.post_type === 'story' ? 'Story': post.media_type), engagement: post.engagement, engagement_rate: parseFloat((post.engagement_rate*100).toString().slice(0,5)), impressions: post.impressions, name: post.post_id})
    
        }
    
    }
 
    const handlePagesMenuOpen = event => {
        setAnchorPagesMenu(event.currentTarget);
    };
    const handlePagesMenuClose = (link, event) => {
        setAnchorPagesMenu(null);
    };

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };


    return(
        <div className={classes.root}>
            {/* the fullscreen loading overlay */}
            <Backdrop className={classes.backdrop} open={loading ? true : false }>
                <img src='/logoanimation_klein.png' />
            </Backdrop>
            <Drawer  />
            <main className={classes.content} >
                <div className={classes.toolbar} /> 
                    <Box>
                        <Container maxWidth="xl">
                            <Pickersandfilter handleDateChange={handleDateChange} selectedDate={selectedDate}  setcheckedPages={setcheckedPages} />
                            <Grid container>
                                <Grid item style={{width: "100%"}}>
                                    <Card variant='outlined'>
                                        <CardHeader
                                            className={classes.cardHeader}
                                            title='Published Posts'
                                            subheader='Review the lifetime performance of the posts of all your connected networks you published during the selected period.'
                                        />
                                        <Divider/>
                                        <CardContent>
                                            <TableContainer>
                                     
                                                <Table
                                                    className={classes.table}
                                                    aria-labelledby="tableTitle"
                                                    size="medium"
                                                    aria-label="enhanced table"
                                                >
                                                    <EnhancedTableHead
                                                        classes={classes}
                                                        order={order}
                                                        orderBy={orderBy}
                                                        onRequestSort={handleRequestSort}
                                                    />
                                                    <TableBody>
                                                        {stableSort(rows, getComparator(order, orderBy))
                                                            .map((row, index) => {

                                                            return (
                                                                <TableRow
                                                                    hover                                                                                                                                 
                                                                    tabIndex={-1}
                                                                    key={row.name}
                                                                >
                                                                    <TableCell size="small" align="right" >{row.post.card}</TableCell>
                                                                    <TableCell align="right">{ colorAndIcons[row.network].icon }</TableCell>
                                                                    <TableCell align="right">{row.type}</TableCell>
                                                                    <TableCell align="right">{row.engagement_rate}%</TableCell>
                                                                    <TableCell align="right">{row.impressions}</TableCell>
                                                                    <TableCell align="right">{row.engagement}</TableCell>
                                                                </TableRow>
                                                            );
                                                            })}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </CardContent>
                                    </Card>
                                </Grid>
                            </Grid>
                        </Container>
                    </Box>
            </main>
        </div>
           


    )

}